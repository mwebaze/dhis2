FROM postgres:14
RUN apt-get update && \
	apt-get install --no-install-recommends -y \
	postgis \
	postgresql-14-postgis-3
#COPY ./conf/init.sql /docker-entrypoint-initdb.d/init.sql
EXPOSE 5432
CMD ["postgres"]